package com.lycanitesmobs.client.obj;


import net.minecraft.client.renderer.Vector3f;

public class Mesh
{
    public int[] indices;
    public Vertex[] vertices;
    public Vector3f[] normals;
}
