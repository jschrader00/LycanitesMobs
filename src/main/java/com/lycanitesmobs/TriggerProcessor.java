package com.lycanitesmobs;

import com.lycanitesmobs.core.spawner.trigger.QueuedTrigger;
import com.lycanitesmobs.core.spawner.trigger.QueuedTriggers;

public class TriggerProcessor extends Thread {
    public void run() {
        try {
            LycanitesMobs.logInfo("TriggerProcessor",
                    "TriggerProcessor Thread " + Thread.currentThread().getId() + " is running");
            while (true) {
                if (!QueuedTriggers.isEmpty()) {
                    long start = System.currentTimeMillis();
                    int triggerCnt = QueuedTriggers.length();
                    while (!QueuedTriggers.isEmpty()) {
                        QueuedTrigger qTrigger = QueuedTriggers.dequeue();
                        qTrigger.trigger.trigger(qTrigger.world, qTrigger.player, qTrigger.origin, qTrigger.level,
                                qTrigger.chain);
                    }
                    LycanitesMobs.logInfo("TriggerProcessor", "Processed in " + (System.currentTimeMillis() - start) + " for " + triggerCnt + " triggers");
                }
                Thread.sleep(2000);
            }
        } catch (Exception e) {
            LycanitesMobs.logError("TriggerProcessor Thread run crashed:" + e.getMessage());
        }
    }

}