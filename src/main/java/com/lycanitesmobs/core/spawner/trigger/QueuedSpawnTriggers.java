package com.lycanitesmobs.core.spawner.trigger;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Semaphore;

import com.google.gson.JsonObject;
import com.lycanitesmobs.LycanitesMobs;
import com.lycanitesmobs.core.mobevent.MobEventPlayerServer;
import com.lycanitesmobs.core.spawner.Spawner;
import net.minecraft.world.World;

// Singleton 
public class QueuedSpawnTriggers {
    private static Semaphore lock = new Semaphore(1);
    private static Queue<QueuedSpawnTrigger> queue = new LinkedList<QueuedSpawnTrigger>();

    private QueuedSpawnTriggers() {
    }

    public static void enqueue(QueuedSpawnTrigger trigger) {
        try {
            lock.acquire();
            queue.add(trigger);
        } catch (Exception e) {
            LycanitesMobs.logError("Unable to enqueue QueuedSpawnTrigger");
        } finally {
            lock.release();
        }
    }

    public static QueuedSpawnTrigger dequeue() {
        QueuedSpawnTrigger trigger = null;
        try {
            lock.acquire();
            trigger = queue.remove();
        } catch (Exception e) {
            LycanitesMobs.logError("Unable to dequeue QueuedSpawnTrigger");
        } finally {
            lock.release();
        }
        return trigger;
    }

    // Check thread safety of this
    public static boolean isEmpty() {
        return queue.isEmpty();
    }
}