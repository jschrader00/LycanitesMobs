package com.lycanitesmobs.core.spawner.trigger;

import java.util.LinkedList;
import java.util.Queue;

import com.google.gson.JsonObject;
import com.lycanitesmobs.LycanitesMobs;
import com.lycanitesmobs.core.mobevent.MobEventPlayerServer;
import com.lycanitesmobs.core.spawner.Spawner;
import net.minecraft.world.World;
import java.util.concurrent.Semaphore;

// Singleton 
public class QueuedTriggers {
    private static Semaphore lock = new Semaphore(1);
    private static Queue<QueuedTrigger> queue = new LinkedList<QueuedTrigger>();

    private QueuedTriggers() {
    }

    public static void enqueue(QueuedTrigger trigger) {
        try {
            lock.acquire();
            queue.add(trigger);
        } catch (Exception e) {
            LycanitesMobs.logError("Unable to enqueue QueuedTrigger");
        } finally {
            lock.release();
        }
    }

    public static QueuedTrigger dequeue() {
        QueuedTrigger trigger = null;
        try {
            lock.acquire();
            trigger = queue.remove();
        } catch (Exception e) {
            LycanitesMobs.logError("Unable to dequeue QueuedTrigger");
        } finally {
            lock.release();
        }
        return trigger;
    }

    public static boolean isEmpty() {
        return queue.isEmpty();
    }

    public static int length(){
        return queue.size();
    }
}