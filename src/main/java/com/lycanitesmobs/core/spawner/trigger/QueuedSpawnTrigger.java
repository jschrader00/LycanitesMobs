package com.lycanitesmobs.core.spawner.trigger;

import com.google.gson.JsonObject;
import com.lycanitesmobs.ExtendedWorld;
import com.lycanitesmobs.core.mobevent.MobEventPlayerServer;
import com.lycanitesmobs.core.spawner.MobSpawn;
import com.lycanitesmobs.core.spawner.Spawner;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

// Simple storage for all parameters required to exectue a given trigger for a MobEventSpawnTrigger
// TODO - Add logic that ensures we prevent fucking shit up outside the context of a tick
public class QueuedSpawnTrigger {
    public Spawner spawner;
    public World world;
    public ExtendedWorld worldExt;
    public LivingEntity entityLiving;
    public int level;
    public MobSpawn mobSpawn;
    public PlayerEntity player;
    public int chain;
    public BlockPos spawnPos;

    public QueuedSpawnTrigger(Spawner spawner, World world, ExtendedWorld worldExt, LivingEntity entityLiving, int level,
            MobSpawn mobSpawn, PlayerEntity player, int chain, BlockPos spawnPos) {
        this.spawner = spawner;
        this.world = world;
        this.worldExt = worldExt;
        this.entityLiving = entityLiving;
        this.level = level;
        this.mobSpawn = mobSpawn;
        this.player = player;
        this.chain = chain;
        this.spawnPos = spawnPos;
    }
}