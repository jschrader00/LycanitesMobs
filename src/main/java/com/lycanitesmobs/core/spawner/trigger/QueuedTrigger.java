package com.lycanitesmobs.core.spawner.trigger;

import com.google.gson.JsonObject;
import com.lycanitesmobs.core.mobevent.MobEventPlayerServer;
import com.lycanitesmobs.core.spawner.Spawner;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

// Simple storage for all parameters required to exectue a given trigger for a MobEventSpawnTrigger
// TODO - Add logic that ensures we prevent fucking shit up outside the context of a tick
public class QueuedTrigger {
    public World world;
    public PlayerEntity player;
    public BlockPos origin;
    public int level;
    public int chain;
    public MobEventSpawnTrigger trigger;

    public QueuedTrigger(MobEventSpawnTrigger trigger, World world, PlayerEntity player, BlockPos origin, int level, int chain) {
        this.trigger = trigger;
        this.world = world;
        this.player = player;
        this.origin = origin;
        this.level = level;
        this.chain = chain;
    }
}